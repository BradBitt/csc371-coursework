/**
 * Declares a class representing a 2d grid world for simulating a cellular automaton.
 * Rich documentation for the api and behaviour the World class can be found in world.cpp.
 *
 * The test suites provide granular BDD style (Behaviour Driven Development) test cases
 * which will help further understand the specification you need to code to.
 *
 * @author 902559
 * @date March, 2020
 */
#pragma once

#include "grid.h"

/**
 * Declare the structure of the World class for representing a 2d grid world.
 *
 * A World holds two equally sized Grid objects for the current state and next state.
 *      - These buffers should be swapped using std::swap after each update step.
 */
class World {
    public:
        // Constructors
        World();
        World(unsigned int square_size);
        World(unsigned int width, unsigned int height);
        World(Grid initial_state);
        // Getters
        unsigned int get_width() const;
        unsigned int get_height() const;
        unsigned int get_total_cells() const;
        unsigned int get_alive_cells() const;
        unsigned int get_dead_cells() const;
        const Grid get_state() const;
        // Void
        void resize(unsigned int square_size);
        void resize(unsigned int new_width, unsigned int new_height);
        void step(bool toroidal = false);
        void advance(unsigned int steps, bool toroidal = false);
    private:
        // Variables
        Grid current_state;
        Grid next_state;
        // Other
        unsigned int count_neighbours(int x, int y, bool toroidal) const;
        bool is_valid_coordinate(int coordinate, bool is_x) const;
};