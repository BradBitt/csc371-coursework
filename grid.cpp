/**
 * Implements a class representing a 2d grid of cells.
 *      - New cells are initialized to Cell::DEAD.
 *      - Grids can be resized while retaining their contents in the remaining area.
 *      - Grids can be rotated, cropped, and merged together.
 *      - Grids can return counts of the alive and dead cells.
 *      - Grids can be serialized directly to an ascii std::ostream.
 *
 * You are encouraged to use STL container types as an underlying storage mechanism for the grid cells.
 *
 * @author 902559
 * @date March, 2020
 */
#include "grid.h"

/**
 * Grid::Grid()
 *
 * Construct an empty grid of size 0x0.
 * Can be implemented by calling Grid::Grid(square_size) constructor.
 *
 * @example
 *
 *      // Make a 0x0 empty grid
 *      Grid grid;
 *
 */
Grid::Grid() : width(0), height(0) {}

/**
 * Grid::Grid(square_size)
 *
 * Construct a grid with the desired size filled with dead cells.
 * Single value constructors should be marked "explicit" to prevent them
 * being used to implicitly cast ints to grids on construction.
 *
 * Can be implemented by calling Grid::Grid(width, height) constructor.
 *
 * @example
 *
 *      // Make a 16x16 grid
 *      Grid x(16);
 *
 *      // Also make a 16x16 grid
 *      Grid y = Grid(16);
 *
 *      // This should be a compiler error! We want to prevent this from being allowed.
 *      Grid z = 16;
 *
 * @param square_size
 *      The edge size to use for the width and height of the grid.
 */
Grid::Grid(unsigned int square_size) : width(square_size), height(square_size) {
    this->cells = std::vector<Cell>(square_size * square_size);
    initialise_cells();
}

/**
 * Grid::Grid(width, height)
 *
 * Construct a grid with the desired size filled with dead cells.
 *
 * @example
 *
 *      // Make a 16x9 grid
 *      Grid grid(16, 9);
 *
 * @param width
 *      The width of the grid.
 *
 * @param height
 *      The height of the grid.
 */
Grid::Grid(unsigned int _width, unsigned int _height) : width(_width), height(_height) {
    this->cells = std::vector<Cell>(_width * _height);
    initialise_cells();
}

/**
 * Grid::initialise_cells()
 * 
 * Sets all the cells in the grid to dead. This is also the default cell state for a new grid.
 */
void Grid::initialise_cells() {
    for (unsigned int y = 0; y < this->height; y++) {
        for (unsigned int x = 0; x < this->width; x++) {
            this->set(x, y, Cell::DEAD);
        }
    }
}

/**
 * Grid::get_width()
 *
 * Gets the current width of the grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the width of the grid to the console
 *      std::cout << grid.get_width() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the width of the grid to the console
 *      std::cout << read_only_grid.get_width() << std::endl;
 *
 * @return
 *      The width of the grid.
 */
unsigned int Grid::get_width() const {
    return this->width;
}

/**
 * Grid::get_height()
 *
 * Gets the current height of the grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the height of the grid to the console
 *      std::cout << grid.get_height() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the height of the grid to the console
 *      std::cout << read_only_grid.get_height() << std::endl;
 *
 * @return
 *      The height of the grid.
 */
unsigned int Grid::get_height() const {
    return this->height;
}

/**
 * Grid::get_total_cells()
 *
 * Gets the total number of cells in the grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the total number of cells on the grid to the console
 *      std::cout << grid.get_total_cells() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the total number of cells on the grid to the console
 *      std::cout << read_only_grid.get_total_cells() << std::endl;
 *
 * @return
 *      The number of total cells.
 */
unsigned int Grid::get_total_cells() const {
    return this->cells.size();
}

/**
 * Grid::get_alive_cells()
 *
 * Counts how many cells in the grid are alive.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the number of alive cells to the console
 *      std::cout << grid.get_alive_cells() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the number of alive cells to the console
 *      std::cout << read_only_grid.get_alive_cells() << std::endl;
 *
 * @return
 *      The number of alive cells.
 */
unsigned int Grid::get_alive_cells() const {
    return count_cell_state(Cell::ALIVE);
}

/**
 * Grid::get_dead_cells()
 *
 * Counts how many cells in the grid are dead.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Print the number of dead cells to the console
 *      std::cout << grid.get_dead_cells() << std::endl;
 *
 *      // Should also be callable in a constant context
 *      const Grid &read_only_grid = grid;
 *
 *      // Print the number of dead cells to the console
 *      std::cout << read_only_grid.get_dead_cells() << std::endl;
 *
 * @return
 *      The number of dead cells.
 */
unsigned int Grid::get_dead_cells() const {
    return count_cell_state(Cell::DEAD);
}

/**
 * Grid::count_cell_state(value)
 * 
 * This function counts all the cell states with the provided Cell type.
 * 
 * @param value
 *      The cell value to count.
 * 
 * @return
 *      The number of cells of a given type.
 */
unsigned int Grid::count_cell_state(Cell value) const {
    unsigned int cells = 0;
    for (unsigned int y = 0; y < this->height; y++) {
        for (unsigned int x = 0; x < this->width; x++) {
            if (this->get(x, y) == value) {
                cells++;
            } 
        }
    }
    return cells;
}

/**
 * Grid::resize(square_size)
 *
 * Resize the current grid to a new width and height that are equal. The content of the grid
 * should be preserved within the kept region and padded with Grid::DEAD if new cells are added.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Resize the grid to be 8x8
 *      grid.resize(8);
 *
 * @param square_size
 *      The new edge size for both the width and height of the grid.
 */
void Grid::resize(unsigned int square_size) {
    perform_resize(square_size, square_size);
}

/**
 * Grid::resize(width, height)
 *
 * Resize the current grid to a new width and height. The content of the grid
 * should be preserved within the kept region and padded with Grid::DEAD if new cells are added.
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Resize the grid to be 2x8
 *      grid.resize(2, 8);
 *
 * @param new_width
 *      The new width for the grid.
 *
 * @param new_height
 *      The new height for the grid.
 */
void Grid::resize(unsigned int width, unsigned int height) {
    perform_resize(width, height);
}

/**
 * Grid::perform_resize(width, height)
 * 
 * This is a private helper function that resizes the current grid.
 * This avoids code diplication.
 * 
 * @param new_width
 *      The new width for the grid.
 *
 * @param new_height
 *      The new height for the grid.
 */
void Grid::perform_resize(unsigned int width, unsigned int height) {
    // Moves the old cells over.
    std::vector<Cell> oldCells = this->cells;
    for (unsigned int y = 0; y < this->height; y++) {
        for (unsigned int x = 0; x < this->width; x++) {
            oldCells[this->get_index(x, y)] = this->get(x, y);
        }
    }

    // Assigns the old width and height and updates the new ones.
    unsigned int old_width = this->width;
    unsigned int old_height = this->height;
    this->width = width;
    this->height = height;
    
    // A new list of cells and moves the cell values over.
    this->cells = std::vector<Cell>(this->width * this->height);
    for (unsigned int y = 0; y < height; y++) {
        for (unsigned int x = 0; x < width; x++) {
            if (x < old_width && y < old_height) {
                this->set(x, y, oldCells[(y * old_width) + x]);
            } else {
                this->set(x, y, Cell::DEAD);
            }
        }
    }
}

/**
 * Grid::get_index(x, y)
 *
 * Private helper function to determine the 1d index of a 2d coordinate.
 * Should not be visible from outside the Grid class.
 * The function should be callable from a constant context.
 *
 * @param x
 *      The x coordinate of the cell.
 *
 * @param y
 *      The y coordinate of the cell.
 *
 * @return
 *      The 1d offset from the start of the data array where the desired cell is located.
 */
unsigned int Grid::get_index(unsigned int x, unsigned int y) const {
    return (y * this->width) + x; 
}

/**
 * Grid::get(x, y)
 *
 * Returns the value of the cell at the desired coordinate.
 * Specifically this function should return a cell value, not a reference to a cell.
 * The function should be callable from a constant context.
 * Should be implemented by invoking Grid::operator()(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Read the cell at coordinate (1, 2)
 *      Cell cell = grid.get(1, 2);
 *
 * @param x
 *      The x coordinate of the cell to update.
 *
 * @param y
 *      The y coordinate of the cell to update.
 *
 * @return
 *      The value of the desired cell. Should only be Grid::ALIVE or Grid::DEAD.
 *
 * @throws
 *      std::exception or sub-class if x,y is not a valid coordinate within the grid.
 */
enum Cell Grid::get(unsigned int x, unsigned int y) const {
    try {
        return Grid::operator()(x, y);
    } catch (const std::runtime_error &ex) {
        throw std::runtime_error("Coordinate not valid!");
    }
}

/**
 * Grid::set(x, y, value)
 *
 * Overwrites the value at the desired coordinate.
 * Should be implemented by invoking Grid::operator()(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Assign to a cell at coordinate (1, 2)
 *      grid.set(1, 2, Cell::ALIVE);
 *
 * @param x
 *      The x coordinate of the cell to update.
 *
 * @param y
 *      The y coordinate of the cell to update.
 *
 * @param value
 *      The value to be written to the selected cell.
 *
 * @throws
 *      std::exception or sub-class if x,y is not a valid coordinate within the grid.
 */
void Grid::set(unsigned int x, unsigned int y, enum Cell value) {
    try {
        Grid::operator()(x, y) = value;
    } catch (const std::runtime_error &ex) {
        throw std::runtime_error("Coordinate not valid!");
    }
}

/**
 * Grid::operator()(x, y)
 *
 * Gets a modifiable reference to the value at the desired coordinate.
 * Should be implemented by invoking Grid::get_index(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Get access to read a cell at coordinate (1, 2)
 *      Cell cell = grid(1, 2);
 *
 *      // Directly assign to a cell at coordinate (1, 2)
 *      grid(1, 2) = Cell::ALIVE;
 *
 *      // Extract a reference to an individual cell to avoid calculating it's
 *      // 1d index multiple times if you need to access the cell more than once.
 *      Cell &cell_reference = grid(1, 2);
 *      cell_reference = Cell::DEAD;
 *      cell_reference = Cell::ALIVE;
 *
 * @param x
 *      The x coordinate of the cell to access.
 *
 * @param y
 *      The y coordinate of the cell to access.
 *
 * @return
 *      A modifiable reference to the desired cell.
 *
 * @throws
 *      std::runtime_error or sub-class if x,y is not a valid coordinate within the grid.
 */
Cell& Grid::operator()(const unsigned int x, const unsigned int y) {
    // Gets the x and y range of valid values.
    unsigned int x_range = this->width - 1;
    unsigned int y_range = this->height - 1;
    if (x < 0 || x > x_range || y < 0 || y > y_range) {
        throw std::runtime_error("The coordinate is not valid!");
    }

    return cells[this->get_index(x, y)];
}

/**
 * Grid::operator()(x, y)
 *
 * Gets a read-only reference toth e value at the desired coordinate.
 * The operator should be callable from a constant context.
 * Should be implemented by invoking Grid::get_index(x, y).
 *
 * @example
 *
 *      // Make a grid
 *      Grid grid(4, 4);
 *
 *      // Constant reference to a grid (does not make a copy)
 *      const Grid &read_only_grid = grid;
 *
 *      // Get access to read a cell at coordinate (1, 2)
 *      Cell cell = read_only_grid(1, 2);
 *
 * @param x
 *      The x coordinate of the cell to access.
 *
 * @param y
 *      The y coordinate of the cell to access.
 *
 * @return
 *      A read-only reference to the desired cell.
 *
 * @throws
 *      std::exception or sub-class if x,y is not a valid coordinate within the grid.
 */
Cell Grid::operator()(const unsigned int x, const unsigned int y) const {
    // Gets the x and y range of valid values.
    unsigned int x_range = this->width - 1;
    unsigned int y_range = this->height - 1;
    if (x < 0 || x > x_range || y < 0 || y > y_range) {
        throw std::runtime_error("The coordinate is not valid!");
    }

    return cells[this->get_index(x, y)];
}

/**
 * Grid::crop(x0, y0, x1, y1)
 *
 * Extract a sub-grid from a Grid.
 * The cropped grid spans the range [x0, x1) by [y0, y1) in the original grid.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a grid
 *      Grid y(4, 4);
 *
 *      // Crop the centre 2x2 in y, trimming a 1 cell border off all sides
 *      Grid x = y.crop(x, 1, 1, 3, 3);
 *
 * @param x0
 *      Left coordinate of the crop window on x-axis.
 *
 * @param y0
 *      Top coordinate of the crop window on y-axis.
 *
 * @param x1
 *      Right coordinate of the crop window on x-axis (1 greater than the largest index).
 *
 * @param y1
 *      Bottom coordinate of the crop window on y-axis (1 greater than the largest index).
 *
 * @return
 *      A new grid of the cropped size containing the values extracted from the original grid.
 *
 * @throws
 *      std::exception or sub-class if x0,y0 or x1,y1 are not valid coordinates within the grid
 *      or if the crop window has a negative size.
 */
Grid Grid::crop(int x0, int y0, int x1, int y1) const {
    // Checks if a negative size would be created.
    if ((x1 - x0 < 0) || (y1 - y0 < 0)) {
        throw std::runtime_error("The crop window has a negative size!");
    }

    // Checks if any values are negative.
    if (x0 < 0 || x1 < 0 || y0 < 0 || y1 < 0) {
        throw std::runtime_error("No negative values can be used!");
    }

    Grid newGrid(x1 - x0, y1 - y0);

    try {
        // Loops through the sub grid and copies the values in the cells over.
        for (int y = y0; y < y1; y++) {
            for (int x = x0; x < x1; x++) {
                // Minusing 'x' from 'x0' is a much better way than creating a new variable
                // and incrementing that variable.
                newGrid.set(x - x0, y - y0, this->get(x, y));
            }
        }
    } catch(const std::runtime_error &ex) {
        throw std::runtime_error("Coordinate not valid!");
    }
    
    return newGrid;
}

/**
 * Grid::merge(other, x0, y0, alive_only = false)
 *
 * Merge two grids together by overlaying the other on the current grid at the desired location.
 * By default merging overwrites all cells within the merge reason to be the value from the other grid.
 *
 * Conditionally if alive_only = true perform the merge such that only alive cells are updated.
 *      - If a cell is originally dead it can be updated to be alive from the merge.
 *      - If a cell is originally alive it cannot be updated to be dead from the merge.
 *
 * @example
 *
 *      // Make two grids
 *      Grid x(2, 2), y(4, 4);
 *
 *      // Overlay x as the upper left 2x2 in y
 *      y.merge(x, 0, 0);
 *
 *      // Overlay x as the bottom right 2x2 in y, reading only alive cells from x
 *      y.merge(x, 2, 2, true);
 *
 * @param other
 *      The other grid to merge into the current grid.
 *
 * @param x0
 *      The x coordinate of where to place the top left corner of the other grid.
 *
 * @param y0
 *      The y coordinate of where to place the top left corner of the other grid.
 *
 * @param alive_only
 *      Optional parameter. If true then merging only sets alive cells to alive but does not explicitly set
 *      dead cells, allowing whatever value was already there to persist. Defaults to false.
 *
 * @throws
 *      std::exception or sub-class if the other grid being placed does not fit within the bounds of the current grid.
 */
void Grid::merge(const Grid& other, int x0, int y0, bool alive_only /*= false*/) {
    // Checks that the boundaries fit within the current Grid
    if (other.get_width() > this->width - x0 || other.get_height() > this->height - y0) {
        throw std::runtime_error("Other grid being placed does not fit within the bounds of the current grid!");
    }

    // Checks if any of the co-ordinates are negative.
    if (x0 < 0 || y0 < 0) {
        throw std::runtime_error("Negative co-ordinates cannot be used!");
    }

    // Loops through the inner grid.
    for (unsigned int y = y0; y < other.get_height() + y0; y++) {
        for (unsigned int x = x0; x < other.get_width() + x0; x++) {
            // If bool is true, then only update cell if the cell in the other grid is true.
            if (alive_only) {
                if (other.get(x - x0, y - y0) == Cell::ALIVE) {
                    this->set(x, y, Cell::ALIVE);
                }
            } else {
                this->set(x, y, other.get(x - x0, y - y0));
            }
        }
    }
}

/**
 * Grid::rotate(rotation)
 *
 * Create a copy of the grid that is rotated by a multiple of 90 degrees.
 * The rotation can be any integer, positive, negative, or 0.
 * The function should take the same amount of time to execute for any valid integer input.
 * The function should be callable from a constant context.
 *
 * @example
 *
 *      // Make a 1x3 grid
 *      Grid x(1,3);
 *
 *      // y is size 3x1
 *      Grid y = x.rotate(1);
 *
 * @param _rotation
 *      An positive or negative integer to rotate by in 90 intervals.
 *
 * @return
 *      Returns a copy of the grid that has been rotated.
 */
Grid Grid::rotate(int rotation) const {
    // If the rotation is 0 then return the main Grid.
    // This is so nothing is created and wasted.
    if (rotation == 0) {
        return * this;
    }

    // Creates the new grid to return and
    // copies over cells from main grid.
    Grid newGrid(this->width, this->height);
    for (unsigned int y = 0; y < this->height; y++) {
        for (unsigned int x = 0; x < this->width; x++) {
            newGrid.set(x, y, this->get(x, y));
        }
    }
    
    // Runs a loop based on number of rotations, either positive or negative.
    for (int i = rotation; i != 0; rotation < 0 ? i++ : i--) {

        // Creates a temp grid
        Grid tempGrid(newGrid.width, newGrid.height);
        for (unsigned int y = 0; y < newGrid.height; y++) {
            for (unsigned int x = 0; x < newGrid.width; x++) {
                tempGrid.set(x, y, newGrid.get(x, y));
            }
        }

        // Updates the height and width of the new grid.
        newGrid.width = tempGrid.height;
        newGrid.height = tempGrid.width;
        
        // Rotate clockwise
        if (i > 0) {
            for (unsigned int y = 0; y < tempGrid.height; y++) {
                for (unsigned int x = 0; x < tempGrid.width; x++) {
                    newGrid.set(y, x, tempGrid.get(x, tempGrid.height - y - 1));
                }
            }
        
        // Rotate anticlockwise
        } else {
            for (unsigned int y = 0; y < tempGrid.height; y++) {
                for (unsigned int x = 0; x < tempGrid.width; x++) {
                    newGrid.set(y, x, tempGrid.get(tempGrid.width - x - 1, y));
                }
            }
        }
    }

    return newGrid;
}

/**
 * operator<<(output_stream, grid)
 *
 * Serializes a grid to an ascii output stream.
 * The grid is printed wrapped in a border of - (dash), | (pipe), and + (plus) characters.
 * Alive cells are shown as # (hash) characters, dead cells with ' ' (space) characters.
 *
 * The function should be callable on a constant Grid.
 *
 * @example
 *
 *      // Make a 3x3 grid with a single alive cell
 *      Grid grid(3);
 *      grid(1, 1) = Cell::ALIVE;
 *
 *      // Print the grid to the console
 *      std::cout << grid << std::endl;
 *
 *      // The grid is printed with a border of + - and |
 *
 *      +---+
 *      |   |
 *      | # |
 *      |   |
 *      +---+
 *
 * @param os
 *      An ascii mode output stream such as std::cout.
 *
 * @param grid
 *      A grid object containing cells to be printed.
 *
 * @return
 *      Returns a reference to the output stream to enable operator chaining.
 */
std::ostream& operator<<(std::ostream& output_stream, Grid& grid) {
    std::string sideChar = "|";
    std::string endChar = "+";
    std::string middleChar = "-";
    std::string noContentLine;

    // Creates top and bottom string.
    noContentLine.append(endChar);
    for (unsigned int i = 0; i < grid.get_width(); i++) {
        noContentLine.append(middleChar);
    }
    noContentLine.append(endChar);
    noContentLine.append("\n");
    
    // Creates the content string.
    std::string content;
    for (unsigned int y = 0; y < grid.get_height(); y++) {

        // Adds first char.
        content.append(sideChar);

        // Loops through all cells on row.
        for (unsigned int x = 0; x < grid.get_width(); x++) {
            // Appends the cell state to the string.
            grid.get(x, y) == Cell::DEAD ? content.append(" ") : content.append("#");
        }

        // Adds end char.
        content.append(sideChar);

        // Adds newline char.
        content.append("\n");
    }

    // Combines all the strings.
    std::string returnString;
    returnString.append(noContentLine);
    returnString.append(content);
    returnString.append(noContentLine);

    output_stream << returnString;
    return output_stream;
}