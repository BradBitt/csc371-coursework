/**
 * Declares a class representing a 2d grid of cells.
 * Rich documentation for the api and behaviour the Grid class can be found in grid.cpp.
 *
 * The test suites provide granular BDD style (Behaviour Driven Development) test cases
 * which will help further understand the specification you need to code to.
 *
 * @author 902559
 * @date March, 2020
 */
#pragma once

#include <exception>
#include <iostream>
#include <vector>

/**
 * A Cell is a char limited to two named values for Cell::DEAD and Cell::ALIVE.
 */
enum Cell : char {
    DEAD  = ' ', // 32 in Asci
    ALIVE = '#' // 35 in Asci
};

/**
 * Declare the structure of the Grid class for representing a 2d grid of cells.
 */
class Grid {
    public:
        // Constructors
        Grid();
        Grid(unsigned int square_size);
        Grid(unsigned int width, unsigned int height);
        // Getters
        unsigned int get_width() const;
        unsigned int get_height() const;
        unsigned int get_total_cells() const;
        unsigned int get_alive_cells() const;
        unsigned int get_dead_cells() const;
        // Void
        void resize(unsigned int square_size);
        void resize(unsigned int width, unsigned int height);
        void set(unsigned int x, unsigned int y, enum Cell value);
        void merge(const Grid& other, int x0, int y0, bool alive_only = false); // default value only needs to be mentioned in header file.
        // Operators
        Cell& operator()(const unsigned int x, const unsigned int y);
        Cell operator()(const unsigned int x, const unsigned int y) const;
        friend std::ostream& operator<<(std::ostream& output_stream, Grid& grid);
        // Other
        enum Cell get(unsigned int x, unsigned int y) const;
        Grid crop(int x0, int y0, int x1, int y1) const;
        Grid rotate(int rotation) const;
    private:
        // Variables
        std::vector<Cell> cells;
        unsigned int width, height;
        // Getters
        unsigned int get_index(unsigned int x, unsigned int y) const;
        // Custom helper functions
        unsigned int count_cell_state(Cell value) const;
        void perform_resize(unsigned int width, unsigned int height);
        void initialise_cells();
};
