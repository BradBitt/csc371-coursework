/**
 * Implements a Zoo namespace with methods for constructing Grid objects containing various creatures in the Game of Life.
 *      - Creatures like gliders, light weight spaceships, and r-pentominos can be spawned.
 *          - These creatures are drawn on a Grid the size of their bounding box.
 *
 *      - Grids can be loaded from and saved to an ascii file format.
 *          - Ascii files are composed of:
 *              - A header line containing an integer width and height separated by a space.
 *              - followed by (height) number of lines, each containing (width) number of characters,
 *                terminated by a newline character.
 *              - (space) ' ' is Cell::DEAD, (hash) '#' is Cell::ALIVE.
 *
 *      - Grids can be loaded from and saved to an binary file format.
 *          - Binary files are composed of:
 *              - a 4 byte int representing the grid width
 *              - a 4 byte int representing the grid height
 *              - followed by (width * height) number of individual bits in C-style row/column format,
 *                padded with zero or more 0 bits.
 *              - a 0 bit should be considered Cell::DEAD, a 1 bit should be considered Cell::ALIVE.
 *
 * @author 902559
 * @date March, 2020
 */
#include "zoo.h"

/**
 * Zoo::glider()
 *
 * Construct a 3x3 grid containing a glider.
 * https://www.conwaylife.com/wiki/Glider
 *
 * @example
 *
 *      // Print a glider in a Grid the size of its bounding box.
 *      std::cout << Zoo::glider() << std::endl;
 *
 *      +---+
 *      | # |
 *      |  #|
 *      |###|
 *      +---+
 *
 * @return
 *      Returns a Grid containing a glider.
 */
Grid Zoo::glider() {
    Grid newGrid = Grid(3, 3);

    newGrid.set(1, 0, Cell::ALIVE);
    newGrid.set(2, 1, Cell::ALIVE);
    newGrid.set(0, 2, Cell::ALIVE);
    newGrid.set(1, 2, Cell::ALIVE);
    newGrid.set(2, 2, Cell::ALIVE);

    return newGrid;
}

/**
 * Zoo::r_pentomino()
 *
 * Construct a 3x3 grid containing an r-pentomino.
 * https://www.conwaylife.com/wiki/R-pentomino
 *
 * @example
 *
 *      // Print an r-pentomino in a Grid the size of its bounding box.
 *      std::cout << Zoo::r_pentomino() << std::endl;
 *
 *      +---+
 *      | ##|
 *      |## |
 *      | # |
 *      +---+
 *
 * @return
 *      Returns a Grid containing a r-pentomino.
 */
Grid Zoo::r_pentomino() {
    Grid newGrid = Grid(3, 3);

    newGrid.set(1, 0, Cell::ALIVE);
    newGrid.set(2, 0, Cell::ALIVE);
    newGrid.set(0, 1, Cell::ALIVE);
    newGrid.set(1, 1, Cell::ALIVE);
    newGrid.set(1, 2, Cell::ALIVE);

    return newGrid;
}

/**
 * Zoo::light_weight_spaceship()
 *
 * Construct a 5x4 grid containing a light weight spaceship.
 * https://www.conwaylife.com/wiki/Lightweight_spaceship
 *
 * @example
 *
 *      // Print a light weight spaceship in a Grid the size of its bounding box.
 *      std::cout << Zoo::light_weight_spaceship() << std::endl;
 *
 *      +-----+
 *      | #  #|
 *      |#    |
 *      |#   #|
 *      |#### |
 *      +-----+
 *
 * @return
 *      Returns a grid containing a light weight spaceship.
 */
Grid Zoo::light_weight_spaceship() {
    Grid newGrid = Grid(5, 4);

    newGrid.set(1, 0, Cell::ALIVE);
    newGrid.set(4, 0, Cell::ALIVE);
    newGrid.set(0, 1, Cell::ALIVE);
    newGrid.set(0, 2, Cell::ALIVE);
    newGrid.set(4, 2, Cell::ALIVE);
    newGrid.set(0, 3, Cell::ALIVE);
    newGrid.set(1, 3, Cell::ALIVE);
    newGrid.set(2, 3, Cell::ALIVE);
    newGrid.set(3, 3, Cell::ALIVE);

    return newGrid;
}

/**
 * Zoo::load_ascii(path)
 *
 * Load an ascii file and parse it as a grid of cells.
 * Should be implemented using std::ifstream.
 *
 * @example
 *
 *      // Load an ascii file from a directory
 *      Grid grid = Zoo::load_ascii("path/to/file.gol");
 *
 * @param path
 *      The std::string path to the file to read in.
 *
 * @return
 *      Returns the parsed grid.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if:
 *          - The file cannot be opened.
 *          - The parsed width or height is not a positive integer.
 *          - Newline characters are not found when expected during parsing.
 *          - The character for a cell is not the ALIVE or DEAD character.
 */
Grid Zoo::load_ascii(std::string path) {
    std::ifstream ifs;
    try {
        ifs.open(path, std::ifstream::in);
    } catch (const std::runtime_error &ex) {
        throw std::runtime_error("Unable to open file!");
    }

    // Checks the file or directory exists
    if (!ifs) {
        throw std::runtime_error("File does not exist!");
    }

    // Reads the first line.
    char asci_value = ifs.get();
    // Checks if the asci value passed in, is a number.
    if (!(asci_value >= 48 && asci_value <= 57)) {
        throw std::runtime_error("Invalid input for grid width!");
    }
    unsigned int width = asci_value - 48;
    ifs.get();
    asci_value = ifs.get();
    // Checks if the asci value passed in, is a number.
    if (!(asci_value >= 48 && asci_value <= 57)) {
        throw std::runtime_error("Invalid input for grid Height!");
    }
    unsigned int height = asci_value - 48;

    // Creates the Grid.
    Grid newGrid = Grid(width, height);

    // Reads in the chars line by line
    for (unsigned int y = 0; y < newGrid.get_height(); y++) {

        // Skips the new line chars.
        // An infinite loop that continues untill it finds a Carriage return char
        if (ifs.get() != 13) {
            throw std::runtime_error("Newline character not found when expected during parsing!");
        }
        ifs.get(); // the new line char.

        for (unsigned int x = 0; x < newGrid.get_width(); x++) {
            // If the ASCI char is equal to alive.
            // Then change the cell to alive.
            char asci_cell_value = ifs.get();
            if (asci_cell_value == Cell::ALIVE) {
                newGrid.set(x, y, Cell::ALIVE);
            } else if (asci_cell_value != Cell::DEAD) {
                throw std::runtime_error("The character for a cell is not the ALIVE or DEAD character!");
            }
        }
    }

    // Closes the stream one finished.
    ifs.close();
    return newGrid;
}

/**
 * Zoo::save_ascii(path, grid)
 *
 * Save a grid as an ascii .gol file according to the specified file format.
 * Should be implemented using std::ofstream.
 *
 * @example
 *
 *      // Make an 8x8 grid
 *      Grid grid(8);
 *
 *      // Save a grid to an ascii file in a directory
 *      try {
 *          Zoo::save_ascii("path/to/file.gol", grid);
 *      }
 *      catch (const std::exception &ex) {
 *          std::cerr << ex.what() << std::endl;
 *      }
 *
 * @param path
 *      The std::string path to the file to write to.
 *
 * @param grid
 *      The grid to be written out to file.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if the file cannot be opened.
 */
void Zoo::save_ascii(std::string path, Grid grid) {
    std::ofstream ofs;
    try {
        ofs.open(path, std::ofstream::out);
    } catch (const std::runtime_error &ex) {
        throw std::runtime_error("Unable to open file!");
    }

    // Checks the file or directory exists
    if (!ofs) {
        throw std::runtime_error("File does not exist!");
    }

    // Writes the first line for the file.
    ofs << grid.get_width() << " " << grid.get_height() << std::endl;

    // Writes the values of the cells to the file.
    for (unsigned int y = 0; y < grid.get_height(); y++) {
        for (unsigned int x = 0; x < grid.get_width(); x++) {
            if (grid.get(x, y) == Cell::ALIVE) {
                ofs << "#";
            } else {
                ofs << " ";
            }
        }
        ofs << std::endl;
    }

    // Closes the file.
    ofs.close();
}

/**
 * Zoo::load_binary(path)
 *
 * Load a binary file and parse it as a grid of cells.
 * Should be implemented using std::ifstream.
 *
 * @example
 *
 *      // Load an binary file from a directory
 *      Grid grid = Zoo::load_binary("path/to/file.bgol");
 *
 * @param path
 *      The std::string path to the file to read in.
 *
 * @return
 *      Returns the parsed grid.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if:
 *          - The file cannot be opened.
 *          - The file ends unexpectedly.
 */
Grid Zoo::load_binary(std::string path) {
    std::ifstream ifs;
    try {
        ifs.open(path, std::ifstream::binary);
    } catch (const std::runtime_error &ex) {
        throw std::runtime_error("Unable to open file!");
    }

    // Checks the file or directory exists
    if (!ifs) {
        throw std::runtime_error("File does not exist!");
    }

    // Initalises values.
    unsigned int width = 0;
    unsigned int height = 0;
    char all_cell_values [8];

    // Reads in all data from file.
    // Reads the first line.
    ifs.read((char*) &width, sizeof(int));
    ifs.read((char*) &height, sizeof(int));
    // Reads in the cell values.
    ifs.read((char*) &all_cell_values, 8);

    // Checks if the file is still open after reading in data.
    if (!ifs) {
        throw std::runtime_error("The file ended unexpectedly!");
    }

    // Creates the grid with the above properties.
    Grid newGrid = Grid(width, height);

    // Reads each bit in each char. If the bit is 0, then the cell is dead, if 1 then the cell is alive.
    int byte_counter = 0;
    int bit_counter = 0;
    for (unsigned int y = 0; y < newGrid.get_height(); y++) {
        for (unsigned int x = 0; x < newGrid.get_width(); x++) {
            // Checks if the given bit is 1.
            int bit = ((all_cell_values[byte_counter] >> bit_counter) & 1);
            if (bit == 1) {
                newGrid.set(x, y, Cell::ALIVE);
            }
            
            // Resets the bit counter if its equal to 7, the last bit in a byte.
            if (bit_counter == 7) {
                bit_counter = 0;
                byte_counter++;
            } else {
                bit_counter++;
            }
        }
    }

    ifs.close();
    return newGrid;
}

/**
 * Zoo::save_binary(path, grid)
 *
 * Save a grid as an binary .bgol file according to the specified file format.
 * Should be implemented using std::ofstream.
 *
 * @example
 *
 *      // Make an 8x8 grid
 *      Grid grid(8);
 *
 *      // Save a grid to an binary file in a directory
 *      try {
 *          Zoo::save_binary("path/to/file.bgol", grid);
 *      }
 *      catch (const std::exception &ex) {
 *          std::cerr << ex.what() << std::endl;
 *      }
 *
 * @param path
 *      The std::string path to the file to write to.
 *
 * @param grid
 *      The grid to be written out to file.
 *
 * @throws
 *      Throws std::runtime_error or sub-class if the file cannot be opened.
 */
void Zoo::save_binary(std::string path, Grid grid) {
    std::ofstream ofs;
    try {
        ofs.open(path, std::ofstream::out);
    } catch (const std::runtime_error &ex) {
        throw std::runtime_error("Unable to open file!");
    }

    // Checks the file or directory exists
    if (!ofs) {
        throw std::runtime_error("File does not exist!");
    }

    // Writes the width and height to file.
    unsigned int width = grid.get_width();
    unsigned int height = grid.get_height();
    ofs.write((char *) &width, sizeof(unsigned int));
    ofs.write((char *) &height, sizeof(unsigned int));

    // Variables needed to write cell values to file.
    char all_cell_values [8];
    char bit_buffer = 0;
    int bit_counter = 0;
    int byte_counter = 0;

    // Gets the cell values from the grid and writes them to the char array as 1's and 0's.
    for (unsigned int y = 0; y < grid.get_height(); y++) {
        for (unsigned int x = 0; x < grid.get_width(); x++) {
            // Gets the cell value.
            Cell cell = grid.get(x, y);

            // Writes 0 as a bit if its dead and writes 1 as a bit if its alive.
            int bit_value = ((cell == Cell::ALIVE) ? 1 : 0);
            bit_buffer |= (bit_value << bit_counter);

            // Adds the char to the array.
            all_cell_values[byte_counter] = bit_buffer;

            // Resets the bit counter if its equal to 7, the last bit in a byte.
            if (bit_counter == 7) {
                bit_counter = 0;
                bit_buffer = 0;
                byte_counter++;
            } else {
                bit_counter++;
            }
        }
    }

    // Writes the cell values to file.
    ofs.write((char*) &all_cell_values, 8);

    // Closes the file.
    ofs.close();
}